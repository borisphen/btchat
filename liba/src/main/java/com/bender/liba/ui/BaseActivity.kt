package com.bender.liba.ui

import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.bender.liba.R

/**
 * @author Boris Rayskiy
 *
 * Created on Sep 2019
 */
abstract class BaseActivity : AppCompatActivity(), NavigationManager {
    open lateinit var navController: NavController

    companion object {
        const val TIME_TO_EXIT = 2000L
    }

    override fun goBack() {
        if (!navController.popBackStack()) {
            super.onBackPressed()
        }
    }

    override fun navigate(resId: Int, args: Bundle?) {
        navController.navigate(resId, args)
    }

    override fun onBackPressed() {
        manageBackPress()
    }

    private fun manageBackPress() {
        getForegroundFragment()?.let {
            when (it) {
                is MainStateFragment<*> -> {
                    exit()
                }
                is ModalStateFragment<*>, is LoginStateFragment<*> -> (it as NavigationFragment<*>).goBack()
                else -> super.onBackPressed()
            }
            return
        }
    }

    private fun getForegroundFragment(): Fragment? {
        val navHostFragment =
            supportFragmentManager.findFragmentById(getNavControllerId()) as NavHostFragment? // Hostfragment
        return navHostFragment?.childFragmentManager?.fragments?.get(0)
    }

    private var doubleBackToExitPressedOnce = false
    private val exitHandler = Handler()

    private val exitRunnable: Runnable = Runnable {
        doubleBackToExitPressedOnce = false
    }

    override fun exit() {
        if (doubleBackToExitPressedOnce) {
            finish()
            return
        }
        doubleBackToExitPressedOnce = true
        Toast.makeText(this, getString(R.string.exitMessage), Toast.LENGTH_SHORT).show()
        exitHandler.postDelayed(exitRunnable, TIME_TO_EXIT)
    }

    override fun onPause() {
        super.onPause()
        exitHandler.removeCallbacksAndMessages(null)
    }

    abstract fun getNavControllerId(): Int
}
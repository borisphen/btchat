package com.bender.liba.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.bender.liba.utils.KeyboardUtils

/**
 * @author Boris Rayskiy
 *
 * Created on Jun 2019
 */
abstract class BaseFragment<B : ViewDataBinding> : Fragment() {

    protected var dataBinding: B? = null

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        KeyboardUtils.hideKeyboard(activity!!)
        dataBinding = DataBindingUtil.inflate(layoutInflater, getLayoutId(), container, false)
        dataBinding?.lifecycleOwner = this
        setHasOptionsMenu(true)
        return dataBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init(savedInstanceState)
    }

    abstract fun getLayoutId(): Int

    abstract fun getTitleId(): Int?

    abstract fun init(savedInstanceState: Bundle?)

    protected fun showError(error: String) {
        Toast.makeText(activity, error, Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        dataBinding = null
    }
}
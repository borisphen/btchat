package com.bender.liba.ui

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding

/**
 * @author Boris Rayskiy
 *
 * Created on Jun 2019
 */

enum class ScreenState(val value: Int) {
    LOGIN(0),
    MAIN(1),
    MODAL(2),
    ONBOARDING(3)
}

abstract class NavigationFragment<T : ViewDataBinding> : BaseFragment<T>(), ToolbarManager, NavigationManager {

    private var toolbarManager: ToolbarManager? = null
    private var navigationManager: NavigationManager? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            toolbarManager = context as ToolbarManager
        } catch (ignore: ClassCastException) { }
        try {
            navigationManager = context as NavigationManager
        } catch (ignore: ClassCastException) { }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getTitleId()?.let {
            setTitle(it)
        }
        setScreenState(getState())
    }

    override fun goBack() {
        navigationManager?.goBack()
    }

    override fun navigate(resId: Int, args: Bundle?) {
        navigationManager?.navigate(resId, args)
    }

    override fun exit() {
        navigationManager?.exit()
    }

    override fun setDrawerEnabled(enabled: Boolean) {
        toolbarManager?.setDrawerEnabled(enabled)
    }

    override fun showToolbar(show: Boolean) {
        toolbarManager?.showToolbar(show)
    }

    override fun showHamburgerIcon() {
        toolbarManager?.showHamburgerIcon()
    }

    override fun showBackIcon() {
        toolbarManager?.showBackIcon()
    }

    override fun setTitle(titleId: Int) {
        titleId.let {
            if (it > 0) {
                toolbarManager?.setTitle(it)
            }
        }
    }

    override fun setTitle(title: String) {
        if (title.isNotEmpty())
            toolbarManager?.setTitle(title)
    }

    override fun setDrawerSelection(index: Int) {
        toolbarManager?.setDrawerSelection(index)
    }

    override fun showBottomNavigation(show: Boolean) {
        toolbarManager?.showBottomNavigation(show)
    }

    override fun setScreenState(state: ScreenState) {
        toolbarManager?.setScreenState(state)
    }

    override fun showToolbarLogo() {
        toolbarManager?.showToolbarLogo()
    }

    override fun showToolbarTitle() {
        toolbarManager?.showToolbarTitle()
    }

    abstract fun getState(): ScreenState
}
package com.bender.liba.ui

/**
 * @author Boris Rayskiy
 *
 * Created on Dec 2019
 */
interface ToolbarManager {
    fun setDrawerEnabled(enabled: Boolean)
    fun showBottomNavigation(show: Boolean)
    fun showToolbar(show: Boolean)
    fun showHamburgerIcon()
    fun showBackIcon()
    fun setTitle(titleId: Int)
    fun setTitle(title: String)
    fun setDrawerSelection(index: Int)
    fun setScreenState(state: ScreenState)
    fun showToolbarLogo()
    fun showToolbarTitle()
}
package com.bender.liba.ui

import androidx.databinding.ViewDataBinding

/**
 * @author Boris Rayskiy
 *
 * Created on Sep 2019
 */
abstract class ModalStateFragment<T : ViewDataBinding> : NavigationFragment<T>() {
    override fun getState(): ScreenState = ScreenState.MODAL
}
package com.bender.liba.ui

import android.os.Bundle

/**
 * @author Boris Rayskiy
 *
 * Created on Dec 2019
 */
interface NavigationManager {
    fun goBack()
    fun navigate(resId: Int, args: Bundle?)
    fun exit()
}
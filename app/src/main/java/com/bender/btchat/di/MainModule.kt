package com.bender.btchat.di

import com.bender.btchat.bt.BluetoothChatController
import com.bender.btchat.ui.main.MainViewModel
import com.bender.firebase.FirebaseRepository
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
    single { provideBluetoothController() }
    single<FirebaseRepository> { provideFirebase() }

    viewModel { MainViewModel(get(), get()) }
}
fun provideBluetoothController() = BluetoothChatController()
fun provideFirebase() = FirebaseRepository()

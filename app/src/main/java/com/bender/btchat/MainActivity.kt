package com.bender.btchat

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.bender.btchat.ui.main.MainViewModel
import com.bender.firebase.FirebaseRepository
import com.bender.liba.ui.NavigationManager
import com.bender.liba.ui.ScreenState
import com.bender.liba.ui.ToolbarManager
import com.firebase.ui.auth.AuthUI
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainActivity : AppCompatActivity(), NavigationManager, ToolbarManager {

    private val firebaseRepository: FirebaseRepository by inject()

    companion object {
        private const val REQUEST_ACCESS_COARSE_LOCATION: Int = 234
        private const val REQUEST_SIGN_IN = 9001
    }

    private lateinit var navController: NavController
    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        setupNavigation()
        checkPermissions()
    }

    public override fun onStart() {
        super.onStart()

        // Start sign in if necessary
        if (firebaseRepository.shouldStartSignIn()) {
            startSignIn()
            return
        }
    }

    private fun startSignIn() {
        // Sign in with FirebaseUI
        val intent = AuthUI.getInstance().createSignInIntentBuilder()
            .setAvailableProviders(listOf(AuthUI.IdpConfig.EmailBuilder().build()))
            .setIsSmartLockEnabled(false)
            .build()

        startActivityForResult(intent, REQUEST_SIGN_IN)
    }

    private fun setupNavigation() {
        navController = findNavController(R.id.mainNavigationFragment)
    }

    override fun goBack() {
        navController.popBackStack()
    }

    override fun navigate(resId: Int, args: Bundle?) {
        navController.navigate(resId, args)
    }

    override fun exit() {
        finish()
    }

    private fun checkPermissions() {
        val permissionCheck =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_ACCESS_COARSE_LOCATION
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SIGN_IN) {
            if (resultCode != Activity.RESULT_OK && firebaseRepository.shouldStartSignIn()) {
                startSignIn()
            } else {
                firebaseRepository.createUser()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Activity.RESULT_OK)
            if (requestCode == REQUEST_ACCESS_COARSE_LOCATION) {
                Timber.d("Permissions GRANTED")
            }
    }

    override fun setDrawerEnabled(enabled: Boolean) {
    }

    override fun showBottomNavigation(show: Boolean) {
    }

    override fun showToolbar(show: Boolean) {
    }

    override fun showHamburgerIcon() {
    }

    override fun showBackIcon() {
    }

    override fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun setDrawerSelection(index: Int) {
    }

    override fun setScreenState(state: ScreenState) {
    }

    override fun showToolbarLogo() {
    }

    override fun showToolbarTitle() {
    }

    override fun setTitle(titleId: Int) {
        supportActionBar?.title = getString(titleId)
    }
}

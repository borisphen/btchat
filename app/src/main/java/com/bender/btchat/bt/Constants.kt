package com.bender.btchat.bt

/**
 * Defines several constants used between [BluetoothChatController] and the UI.
 */
interface Constants {
    companion object {
        // Message types sent from the BluetoothChatController to UI
        const val MESSAGE_STATE_CHANGE = 1
        const val MESSAGE_SEND = 2
        const val MESSAGE_WRITE = 3
        const val MESSAGE_DEVICE_NAME = 4
        const val MESSAGE_TOAST = 5
        const val MESSAGE_USER_ID = 6
    }
}
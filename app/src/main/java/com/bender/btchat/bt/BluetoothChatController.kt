@file:Suppress("BlockingMethodInNonBlockingContext")

package com.bender.btchat.bt

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import com.bender.btchat.R
import com.bender.btchat.utils.SingleLiveEvent
import com.bender.btchat.utils.toByteArray
import com.bender.btchat.utils.toPair
import kotlinx.coroutines.*
import timber.log.Timber
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a holder that listens for
 * incoming connections, a holder for connecting with a device, and a
 * holder for performing data transmissions when connected.
 */
class BluetoothChatController {
    // Member fields
    private val btAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    private var secureAcceptHolder: AcceptHolder? =
        null
    private var insecureAcceptHolder: AcceptHolder? =
        null
    private var connectHolder: ConnectHolder? =
        null
    private var connectedHolder: ConnectedHolder? =
        null

    var messageEvent: SingleLiveEvent<Pair<Int, Any?>>? = null

    /**
     * Return the current connection state.
     */
    @get:Synchronized
    var connectionState: Int
        private set
    private var newState: Int
    /**
     * Update UI title according to the current state of the chat connection
     */
    @Synchronized
    private fun updateUserInterfaceTitle() {
        connectionState = connectionState
        Timber.d("updateUserInterfaceTitle() $newState -> connectionState")
        newState = connectionState
        notifyUi(Constants.MESSAGE_STATE_CHANGE, newState)
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    @Synchronized
    fun start() {
        Timber.d("start")
        // Cancel any holder attempting to make a connection
        cancelConnectHolder()
        // Cancel any holder currently running a connection
        cancelConnectedHolder()
        // Start the holder to listen on a BluetoothServerSocket
        if (secureAcceptHolder == null) {
            secureAcceptHolder = AcceptHolder(true)
            secureAcceptHolder?.run()
        }
        if (insecureAcceptHolder == null) {
            insecureAcceptHolder = AcceptHolder(false)
            insecureAcceptHolder?.run()
        }
        // Update UI title
        updateUserInterfaceTitle()
    }

    /**
     * Start the ConnectHolder to initiate a connection to a remote device.
     *
     * @param device The BluetoothDevice to connect
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    @Synchronized
    fun connect(device: BluetoothDevice, secure: Boolean) {
        Timber.d("connect to: $device")
        // Cancel any holder attempting to make a connection
        if (connectionState == STATE_CONNECTING) {
            cancelConnectHolder()
        }
        // Cancel any holder currently running a connection
        cancelConnectedHolder()
        // Start the holder to connect with the given device
        connectHolder = ConnectHolder(device, secure)
        connectHolder?.run()
        // Update UI title
        updateUserInterfaceTitle()
    }

    /**
     * Start the ConnectedHolder to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been connected
     */
    @Synchronized
    fun connected(
        socket: BluetoothSocket?,
        device: BluetoothDevice,
        socketType: String
    ) {
        Timber.d("connected, Socket Type:$socketType")
        // Cancel the holder that completed the connection
        cancelConnectHolder()
        // Cancel any holder currently running a connection
        cancelConnectedHolder()
        // Cancel the accept holder because we only want to connect to one device
        cancelSecureAcceptHolder()
        cancelInsecureAcceptHolder()
        // Start the holder to manage the connection and perform transmissions
        connectedHolder = ConnectedHolder(socket!!, socketType)
        connectedHolder?.run()
        // Send the name of the connected device back to the UI Activity
        notifyUi(Constants.MESSAGE_DEVICE_NAME, device.name)

        // Update UI title
        updateUserInterfaceTitle()
    }

    /**
     * Stop all holders
     */
    @Synchronized
    fun stop() {
        Timber.d("stop")
        cancelConnectHolder()
        cancelConnectedHolder()
        cancelSecureAcceptHolder()
        cancelInsecureAcceptHolder()
        connectionState = STATE_NONE
        // Update UI title
        updateUserInterfaceTitle()
        messageEvent = null
    }

    /**
     * Write to the ConnectedHolder in an unsynchronized manner
     *
     * @param type Sending data type
     * @param message Sending data converted to String
     * @see ConnectedHolder.write
     */
    fun write(type: Int, message: String?) { // Create temporary object
        val r: ConnectedHolder?
        // Synchronize a copy of the ConnectedHolder
        synchronized(this) {
            if (connectionState != STATE_CONNECTED) return
            r = connectedHolder
        }
        // Perform the write unsynchronized
        r!!.write(type, message)
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private fun connectionFailed() { // Send a failure message back to the Activity
        notifyUi(Constants.MESSAGE_TOAST, R.string.unable_to_connect)
        connectionState = STATE_NONE
        // Update UI title
        updateUserInterfaceTitle()
        // Start the service over to restart listening mode
        start()
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private fun connectionLost() { // Send a failure message back to the Activity
        notifyUi(Constants.MESSAGE_TOAST, R.string.connection_was_lost)
        connectionState = STATE_NONE
        // Update UI title
        updateUserInterfaceTitle()
        // Start the service over to restart listening mode
        start()
    }

    /**
     * This Holder runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private inner class AcceptHolder(secure: Boolean) {
        // The local server socket
        private var mmServerSocket: BluetoothServerSocket? = null
        private var mSocketType: String = ""

        fun run() {
            doAsyncGlobal {
                Timber.d(
                    "Socket Type: $mSocketType BEGIN mAcceptHolder $this"
                )
                var socket: BluetoothSocket?
                // Listen to the server socket if we're not connected
                while (connectionState != STATE_CONNECTED) {
                    socket = try { // This is a blocking call and will only return on a
                        // successful connection or an exception
                        mmServerSocket?.accept()
                    } catch (e: IOException) {
                        Timber.e("Socket Type: $mSocketType accept() failed + $e")
                        break
                    }
                    // If a connection was accepted
                    if (socket != null) {
                        synchronized(this@BluetoothChatController) {
                            when (connectionState) {
                                STATE_LISTEN, STATE_CONNECTING ->  // Situation normal. Start the connected holder.
                                    connected(
                                        socket, socket.remoteDevice,
                                        mSocketType
                                    )
                                STATE_NONE, STATE_CONNECTED ->  // Either not ready or already connected. Terminate new socket.
                                    try {
                                        socket.close()
                                    } catch (e: IOException) {
                                        Timber.e("Could not close unwanted socket$e")
                                    }
                            }
                        }
                    }
                }
                Timber.i("END mAcceptHolder, socket Type: $mSocketType")
            }
        }

        fun cancel() {
            doAsyncGlobal {
                Timber.d("Socket Type $mSocketType cancel $this")
                try {
                    mmServerSocket!!.close()
                } catch (e: IOException) {
                    Timber.e("Socket Type $mSocketType close() of server failed + $e")
                }
            }
        }

        init {
            var tmp: BluetoothServerSocket? = null
            mSocketType = if (secure) "Secure" else "Insecure"
            // Create a new listening server socket
            try {
                tmp = if (secure) {
                    btAdapter.listenUsingRfcommWithServiceRecord(
                        NAME_SECURE,
                        MY_UUID_SECURE
                    )
                } else {
                    btAdapter.listenUsingInsecureRfcommWithServiceRecord(
                        NAME_INSECURE,
                        MY_UUID_INSECURE
                    )
                }
            } catch (e: IOException) {
                Timber.e("Socket Type: $mSocketType listen() failed + $e")
            }
            mmServerSocket = tmp
            connectionState = STATE_LISTEN
        }
    }

    /**
     * This Holder runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private inner class ConnectHolder(private val mmDevice: BluetoothDevice, secure: Boolean) {
        private var mmSocket: BluetoothSocket? = null
        private var mSocketType: String = ""

        fun run() {
            doAsyncGlobal {
                Timber.i("BEGIN mConnectHolder SocketType:$mSocketType")
                // Always cancel discovery because it will slow down a connection
                btAdapter.cancelDiscovery()
                // Make a connection to the BluetoothSocket
                try { // This is a blocking call and will only return on a
                    // successful connection or an exception
                    mmSocket!!.connect()
                } catch (e: IOException) { // Close the socket
                    try {
                        mmSocket!!.close()
                    } catch (e2: IOException) {
                        Timber.e(
                            "unable to close() $mSocketType socket during connection failure $e2"
                        )
                    }
                    connectionFailed()
                }
                // Reset the ConnectHolder because we're done
                synchronized(this@BluetoothChatController) { connectHolder = null }
                // Start the connected holder
                connected(mmSocket, mmDevice, mSocketType)
            }
        }

        fun cancel() {
            doAsyncGlobal {
                try {
                    mmSocket!!.close()
                } catch (e: IOException) {
                    Timber.e("close() of connect $mSocketType socket failed$e")
                }
            }
        }

        init {
            var tmp: BluetoothSocket? = null
            mSocketType = if (secure) "Secure" else "Insecure"
            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = if (secure) {
                    mmDevice.createRfcommSocketToServiceRecord(
                        MY_UUID_SECURE
                    )
                } else {
                    mmDevice.createInsecureRfcommSocketToServiceRecord(
                        MY_UUID_INSECURE
                    )
                }
            } catch (e: IOException) {
                Timber.e("Socket Type: $mSocketType create() failed + $e")
            }
            mmSocket = tmp
            connectionState = STATE_CONNECTING
        }
    }

    /**
     * This Holder runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private inner class ConnectedHolder(
        socket: BluetoothSocket,
        socketType: String
    ) {
        private val mmSocket: BluetoothSocket
        private val mmInStream: InputStream?
        private val mmOutStream: OutputStream?
        fun run() {
            doAsyncGlobal {
                Timber.i("BEGIN mConnectedHolder")
                val buffer = ByteArray(1024)
                // Keep listening to the InputStream while connected
                while (connectionState == STATE_CONNECTED) {
                    try { // Read from the InputStream
                        val bytes = mmInStream!!.read(buffer)
                        val data = buffer.toPair()
                        notifyUi(data.first, data.second)
                    } catch (e: IOException) {
                        Timber.e("disconnected$e")
                        connectionLost()
                        break
                    }
                }
            }
        }

        /**
         * Write to the connected OutStream.
         *
         * @param type - Message Type
         * @param message - message/json string
         */
        fun write(type: Int, message: String?) {
            doAsyncGlobal {
                try {
                    val dataToSend = Pair(type, message)
                    val send = (dataToSend as Pair<Int, String>).toByteArray()
                    mmOutStream!!.write(send)
                } catch (e: IOException) {
                    Timber.e("Exception during write$e")
                }
            }
        }

        fun cancel() {
            doAsyncGlobal {
                try {
                    mmSocket.close()
                } catch (e: IOException) {
                    Timber.e("close() of connect socket failed$e")
                }
            }
        }

        init {
            Timber.d("create ConnectedHolder: $socketType")
            mmSocket = socket
            var tmpIn: InputStream? = null
            var tmpOut: OutputStream? = null
            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.inputStream
                tmpOut = socket.outputStream
            } catch (e: IOException) {
                Timber.e("temp sockets not created$e")
            }
            mmInStream = tmpIn
            mmOutStream = tmpOut
            connectionState = STATE_CONNECTED
        }
    }

    private fun cancelConnectHolder() {
        // Cancel any holder attempting to make a connection
        connectHolder?.let {
            it.cancel()
            connectHolder = null
        }
    }

    private fun cancelConnectedHolder() {
        // Cancel any holder currently running a connection
        connectedHolder?.let {
            it.cancel()
            connectedHolder = null
        }
    }

    private fun cancelSecureAcceptHolder() {
        secureAcceptHolder?.let {
            it.cancel()
            secureAcceptHolder = null
        }
    }

    private fun cancelInsecureAcceptHolder() {
        insecureAcceptHolder?.let {
            it.cancel()
            insecureAcceptHolder = null
        }
    }

    companion object {
        // Debugging
        // Name for the SDP record when creating server socket
        private const val NAME_SECURE = "BluetoothChatSecure"
        private const val NAME_INSECURE = "BluetoothChatInsecure"
        // Unique UUID for this application
        private val MY_UUID_SECURE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66")
        private val MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66")
        // Constants that indicate the current connection state
        const val STATE_NONE = 0 // we're doing nothing
        const val STATE_LISTEN = 1 // now listening for incoming connections
        const val STATE_CONNECTING = 2 // now initiating an outgoing connection
        const val STATE_CONNECTED = 3 // now connected to a remote device
    }

    /**
     * Constructor. Prepares a new BluetoothChat session.
     *
     */
    init {
        connectionState =
            STATE_NONE
        newState = connectionState
    }

    // Do async operation bounded to Application lifecycle
    fun doAsyncGlobal(function: suspend () -> Unit) {
        GlobalScope.launch(exceptionHandler) {
            withContext(dispatcher) {
                function.invoke()
            }
        }
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        Timber.e("Caught! $exception")
    }
    private val dispatcher = Dispatchers.IO

    fun notifyUi(status: Int, value: Any?) {
        messageEvent?.postValue(wrap(status, value))
    }

    private fun wrap(status: Int, value: Any?): Pair<Int, Any?> = Pair(status, value)
}
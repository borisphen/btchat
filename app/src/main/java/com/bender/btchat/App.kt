package com.bender.btchat

import android.app.Application
import android.os.StrictMode
import com.bender.btchat.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * @author Boris Rayskiy
 *
 * Created on Jun 2019
 */
class App : Application() {

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        init()
    }

    private fun init() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        initKoin()
        StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
            .detectAll().build())
    }


    private fun initKoin() {
        startKoin {
            // use AndroidLogger as Koin Logger - default Level.INFO
            androidLogger()
            // use the Android context given there
            androidContext(this@App)
            // load properties from assets/koin.properties file
            androidFileProperties()
            // module list
            modules(mainModule)
        }
    }
}
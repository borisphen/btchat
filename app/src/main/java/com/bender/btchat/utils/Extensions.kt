package com.bender.btchat.utils

import java.io.*

fun Pair<Int, String>.toByteArray(): ByteArray {
    val bos = ByteArrayOutputStream()
    try {
        val out = ObjectOutputStream(bos)
        out.writeObject(this)
        out.flush()
        return bos.toByteArray()
    } finally {
        try {
            bos.close()
        } catch (e: IOException) {
            // ignore close exception
        }
    }
}

fun ByteArray.toPair(): Pair<Int, String> {
    val bis = ByteArrayInputStream(this)
    var oin: ObjectInput? = null
    try {
        oin = ObjectInputStream(bis)
        return oin.readObject() as Pair<Int, String>
    } finally {
        try {
            oin?.close()
        } catch (e: IOException) {
            // ignore close exception
        }
    }
}
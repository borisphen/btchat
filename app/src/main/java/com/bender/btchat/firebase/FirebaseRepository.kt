package com.bender.firebase

import com.bender.btchat.ui.chat.ChatMessage
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import timber.log.Timber

/**
 * @author Boris Rayskiy
 *
 * Created on Oct 2019
 */
class FirebaseRepository {

    companion object {
        const val KEY_TIME = "TIME_KEY"
        const val KEY_HR = "HR_KEY"

        const val ROOM_MAME = "ROOM_MAME"
        const val ROOMS_COLLECTION = "rooms"

        const val USERS_COLLECTION = "users"
        const val KEY_USER_NAME = "userName"
        const val KEY_USER_ID = "userId"

        const val MESSAGES_COLLECTION = "messages"
        const val KEY_MESSAGE = "message"
    }

    private val firestore = FirebaseFirestore.getInstance()
    private var insertMessageTask: Task<DocumentReference>? = null

    init {
        val firebaseDatabase = FirebaseDatabase.getInstance()
        firebaseDatabase.setPersistenceEnabled(true)
        firebaseDatabase.getReference(USERS_COLLECTION).keepSynced(true)
        firebaseDatabase.getReference(MESSAGES_COLLECTION).keepSynced(true)
    }


    fun shouldStartSignIn(): Boolean {
        return FirebaseAuth.getInstance().currentUser == null
    }

    fun getCurrentUserId(): String? {
        return FirebaseAuth.getInstance().currentUser?.uid
    }

    fun getUserName(): String? {
        return FirebaseAuth.getInstance().currentUser?.displayName
    }

    fun createRoom(id: String, name: String) {
        val timeStamp = System.currentTimeMillis()
        firestore.collection(ROOMS_COLLECTION).document(id).set(hashMapOf(KEY_TIME to timeStamp, ROOM_MAME to name))
            .addOnSuccessListener {
                Timber.d("Success!")
            }
            .addOnFailureListener {
                Timber.e("Room NOT Added ${it.localizedMessage}")
            }
    }

    fun createUser() {
        val timeStamp = System.currentTimeMillis()
        firestore.collection(USERS_COLLECTION).document(getCurrentUserId()!!).set(hashMapOf(KEY_TIME to timeStamp, KEY_USER_NAME to getUserName()))
            .addOnSuccessListener {
                Timber.d("Success!")
            }
            .addOnFailureListener {
                Timber.e("User NOT Added ${it.localizedMessage}")
            }
    }

    fun getUserByUID(uid: String): Task<DocumentSnapshot> {
        return firestore.collection(USERS_COLLECTION).document(uid).get()
    }

    fun writeMessage(roomId: String, userUid: String, message: String): String {
        val timeStamp = System.currentTimeMillis()
        val newMessageDbId = firestore.collection(ROOMS_COLLECTION).document(roomId).collection(MESSAGES_COLLECTION).document().id
        firestore.collection(ROOMS_COLLECTION).document(roomId).collection(MESSAGES_COLLECTION)
            .document(newMessageDbId).set(hashMapOf(KEY_TIME to timeStamp, KEY_USER_ID to userUid, KEY_MESSAGE to message))
        return newMessageDbId
    }

    fun updateMessage(roomId: String, userUid: String, chatMessage: ChatMessage) {
        firestore.collection(ROOMS_COLLECTION).document(roomId).collection(MESSAGES_COLLECTION).document(chatMessage.dbId)
            .set(hashMapOf(KEY_TIME to chatMessage.time, KEY_USER_ID to userUid, KEY_MESSAGE to chatMessage.text))
    }

    fun getMessagesQuery(roomId: String) = firestore.collection(ROOMS_COLLECTION).document(roomId).collection(MESSAGES_COLLECTION)
        .orderBy(KEY_TIME, Query.Direction.ASCENDING)
}
package com.bender.btchat.ui.chat

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.bender.btchat.R
import com.bender.btchat.bt.BluetoothChatController
import com.bender.btchat.bt.Constants
import com.bender.btchat.databinding.FragmentBluetoothChatBinding
import com.bender.btchat.databinding.MessageBinding
import com.bender.btchat.firebase.FirestoreAdapter
import com.bender.btchat.ui.main.MainViewModel
import com.bender.firebase.FirebaseRepository
import com.bender.liba.ui.NavigationFragment
import com.bender.liba.ui.ScreenState
import com.bender.liba.utils.MoshiUtils
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.fragment_bluetooth_chat.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import timber.log.Timber


/**
 * This fragment controls Bluetooth to communicate with other devices.
 */
class ChatFragment : NavigationFragment<FragmentBluetoothChatBinding>() {

    private val viewModel: MainViewModel by sharedViewModel()
    private val firebaseRepository: FirebaseRepository by inject()

    /**
     * Name of the connected device
     */
    private var connectedDeviceName: String? = null

    /**
     * User name chat with
     */
    private var companionName: String? = null

    /**
     * Local Bluetooth adapter
     */
    private var bluetoothAdapter: BluetoothAdapter? = null

    /**
     * RecyclerView adapter for the conversation thread
     */
    private var messagesAdapter: MessagesAdapter? = null

    private var actionBar: ActionBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        // Get local Bluetooth adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        // If the adapter is null, then Bluetooth is not supported
        if (bluetoothAdapter == null) {
            Toast.makeText(activity, R.string.bluetooth_not_available, Toast.LENGTH_LONG).show()
            activity!!.finish()
        }
    }

    override fun onStart() {
        super.onStart()
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!bluetoothAdapter!!.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(
                enableIntent,
                REQUEST_ENABLE_BT
            )
            // Otherwise, setup the chat session
        }
        messagesAdapter?.startListening()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.stopChatService()
    }

    override fun onResume() {
        super.onResume()
        viewModel.startChatService()
    }

    override fun onStop() {
        super.onStop()
        messagesAdapter?.stopListening()
    }

    override fun init(savedInstanceState: Bundle?) {
        viewModel.let {
            it.selectedDeviceData.observe(viewLifecycleOwner, Observer { deviceData ->
                connectDevice(deviceData.mac)
            })
            it.toastResIdLiveData.observe(viewLifecycleOwner, Observer { text ->
                Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
            })
            it.messageEvent.observe(viewLifecycleOwner, Observer { notifyData ->
                when (notifyData.first) {
                    Constants.MESSAGE_STATE_CHANGE -> when (notifyData.second) {
                        BluetoothChatController.STATE_CONNECTED -> {
                            setStatus(
                                activity?.getString(
                                    R.string.title_connected_to,
                                    connectedDeviceName
                                )!!
                            )
                            viewModel.sendUserID()
                        }
                        BluetoothChatController.STATE_CONNECTING -> setStatus(R.string.title_connecting)
                        BluetoothChatController.STATE_LISTEN,
                        BluetoothChatController.STATE_NONE -> {
                            setStatus(
                                R.string.title_not_connected
                            )
                            messagesAdapter?.stopListening()
                        }
                    }
                    Constants.MESSAGE_SEND -> {
                        if (notifyData.second is String) {
                            val receivedData = notifyData.second as String
                            val chatMessage: ChatMessage =
                                MoshiUtils.fromJson<ChatMessage>(receivedData)!!
                            viewModel.updateFirebaseMessage(chatMessage)
                        }
                    }
                    Constants.MESSAGE_DEVICE_NAME -> {
                        // save the connected device's name
                        connectedDeviceName = notifyData.second as String
                        activity?.let {
                            Toast.makeText(
                                activity, getString(R.string.connected_to)
                                        + connectedDeviceName, Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    Constants.MESSAGE_USER_ID -> {
                        if (notifyData.second is String) {
                            val userUid = notifyData.second as String
                            viewModel.remoteUserUid = userUid
                            val roomId = viewModel.generateRoomId()
                            viewModel.getUserByUID()
                            it.userNameLiveData.observe(viewLifecycleOwner, Observer { userName ->
                                companionName = userName
                                val roomName = "Chat with $userName"
                                viewModel.addRoom(roomId, roomName)
                                setStatus(roomName)
                                initAdapter()
                            })
                        }
                    }
                    Constants.MESSAGE_TOAST -> if (null != activity) {
                        Toast.makeText(
                            activity, notifyData.second as Int,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            })

            dataBinding?.model = it
        }
        actionBar = (activity as AppCompatActivity?)?.supportActionBar

        // Initialize the chat adapter for the conversation thread
        // Initialize the compose field with a listener for the return key
        outEditText.setOnEditorActionListener(writeListener)
    }

    private fun initAdapter() {
        // Initialize the buffer for outgoing messages

        val itemDecorator = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        val drawable = ContextCompat.getDrawable(activity!!, R.drawable.divider)
        drawable?.let {
            itemDecorator.setDrawable(it)
        }
        rvMessages.addItemDecoration(itemDecorator)
        messagesAdapter = object : MessagesAdapter(viewModel.getMessagesQuery()) {
            override fun onDataChanged() {
                smoothScrollToNewestMessages()
            }
        }

        rvMessages.adapter = messagesAdapter
        messagesAdapter?.startListening()

        rvMessages.addOnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (bottom < oldBottom) {
                smoothScrollToNewestMessages()
            }
        }
    }

    private fun smoothScrollToNewestMessages() {
        rvMessages.postDelayed({
            Timber.e("PRE Adapter items count = ${rvMessages.adapter?.itemCount}")
            rvMessages.adapter?.let {
                Timber.e("POST Adapter items count = ${rvMessages.adapter?.itemCount}")
                if (it.itemCount > 0) {
                    rvMessages.smoothScrollToPosition(it.itemCount - 1)
                }
            }
        }, 100)
    }

    /**
     * Makes this device discoverable for 300 seconds (5 minutes).
     */
    private fun ensureDiscoverable() {
        if (bluetoothAdapter!!.scanMode !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE
        ) {
            val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
            startActivity(discoverableIntent)
        }
    }


    /**
     * The action listener for the EditText widget, to listen for the return key
     */
    private val writeListener = OnEditorActionListener { view, actionId, event ->
        // If the action is a key-up event on the return key, send the message
        if (actionId == EditorInfo.IME_NULL && event.action == KeyEvent.ACTION_UP) {
            viewModel.sendMessage()
        }
        true
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private fun setStatus(resId: Int) {
        actionBar?.setSubtitle(resId)
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private fun setStatus(subTitle: String) {
        actionBar?.subtitle = subTitle
    }

    /**
     * Establish connection with other device
     *
     * @param address remote device mac address
     */
    private fun connectDevice(
        address: String
    ) { // Get the device MAC address
        // Get the BluetoothDevice object
        val device = bluetoothAdapter!!.getRemoteDevice(address)
        // Attempt to connect to the device
        viewModel.connectToRemoteDevice(device)
    }

    override fun onCreateOptionsMenu(
        menu: Menu,
        inflater: MenuInflater
    ) {
        inflater.inflate(R.menu.bluetooth_chat, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.secure_connect_scan -> {
                viewModel.isSecureConnection = true
                navigate(R.id.pairingFragment, null)
                return true
            }
            R.id.insecure_connect_scan -> {
                viewModel.isSecureConnection = false
                navigate(R.id.pairingFragment, null)
                return true
            }
            R.id.discoverable -> {
                // Ensure this device is discoverable by others
                ensureDiscoverable()
                return true
            }
        }
        return false
    }

    override fun getState(): ScreenState {
        return ScreenState.MAIN
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_bluetooth_chat
    }

    override fun getTitleId(): Int? {
        return null
    }


    open inner class MessagesAdapter(query: Query?) :
        FirestoreAdapter<MessagesAdapter.ViewHolder>(query) {

        init {
            setHasStableIds(true)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val viewDataBinding =
                MessageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolder(viewDataBinding)
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val snapshot = getSnapshot(position)
            val data = snapshot.data
            data?.let {
                var message = data[FirebaseRepository.KEY_MESSAGE] as String
                val userId = data[FirebaseRepository.KEY_USER_ID]
                message = if (userId == firebaseRepository.getCurrentUserId()) {
                    "Me: $message"
                } else {
                    "${viewModel.userNameLiveData.value}: $message"
                }
                val chatMessage = ChatMessage(
                    snapshot.id,
                    message,
                    data[FirebaseRepository.KEY_TIME] as Long
                )
//            val message = chatMessages[position]
                holder.dataBinding.setVariable(com.bender.btchat.BR.model, chatMessage)
            }
        }

        init {
            this.setHasStableIds(true)
        }

        inner class ViewHolder(val dataBinding: ViewDataBinding) :
            RecyclerView.ViewHolder(dataBinding.root)

    }

    companion object {
        // Intent request codes
        private const val REQUEST_ENABLE_BT = 3
    }
}
package com.bender.btchat.ui.main

/**
 * @author Boris Rayskiy
 *
 * Created on Dec 2019
 */
data class DeviceData(val name: String?, val mac: String)
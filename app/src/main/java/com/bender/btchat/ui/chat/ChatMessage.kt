package com.bender.btchat.ui.chat

/**
 * @author Boris Rayskiy
 *
 * Created on Dec 2019
 */
data class ChatMessage(val dbId: String, val text: String, val time: Long)
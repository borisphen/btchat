package com.bender.btchat.ui.main

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bender.btchat.R
import com.bender.btchat.databinding.DeviceNameBinding
import com.bender.btchat.databinding.PairingFragmentBinding
import com.bender.liba.ui.NavigationFragment
import com.bender.liba.ui.ScreenState
import kotlinx.android.synthetic.main.pairing_fragment.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import timber.log.Timber

class PairingFragment : NavigationFragment<PairingFragmentBinding>() {

    private val viewModel: MainViewModel by sharedViewModel()

    /**
     * Member fields
     */
    private var btAdapter: BluetoothAdapter? = null

    /**
     * Newly discovered devices
     */
    private var newDevicesAdapter: DevicesAdapter = DevicesAdapter()
    private var progress: MenuItem? = null

    override fun init(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Initialize adapters. One for already paired devices and
        // Register for broadcasts when a device is discovered
        val pairedAdapter = DevicesAdapter()
        rvPairedDevices?.adapter = pairedAdapter
        var filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        activity?.registerReceiver(receiver, filter)
        // Register for broadcasts when discovery has finished
        filter = IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        activity?.registerReceiver(receiver, filter)
        // Get the local Bluetooth adapter
        btAdapter = BluetoothAdapter.getDefaultAdapter()
        // Get a set of currently paired devices
        val pairedDevices = btAdapter?.bondedDevices
        // If there are paired devices, add each one to the ArrayAdapter
        if (pairedDevices!!.size > 0) {
            val devices = ArrayList<DeviceData>()
            for (device in pairedDevices) {
                devices.add(DeviceData(name = device.name, mac = device.address))
            }
            pairedAdapter.setData(devices)
        } else {
            tvPairedDevices.setText(R.string.no_devices_found)
        }

        rvNewDevices?.adapter = newDevicesAdapter

        viewModel.scanButtonLiveData.observe(viewLifecycleOwner, Observer {
            doDiscovery()
            btnScan.visibility = View.GONE
        })

        dataBinding?.model = viewModel
    }

    override fun onCreateOptionsMenu(
        menu: Menu,
        inflater: MenuInflater
    ) {
        inflater.inflate(R.menu.progress, menu)
        progress = menu.findItem(R.id.menu_refresh)
    }

    fun showProgress(show: Boolean) {
        if (show) {
            progress?.setActionView(
                R.layout.actionbar_indeterminate_progress
            )
        } else {
            progress?.actionView = null
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        // Make sure we're not doing discovery anymore
        if (btAdapter != null) {
            btAdapter!!.cancelDiscovery()
        }
        // Unregister broadcast listeners
        activity?.unregisterReceiver(receiver)
    }

    /**
     * Start device discover with the BluetoothAdapter
     */
    private fun doDiscovery() {
        showProgress(true)
        Timber.d("doDiscovery()")
        // Indicate scanning in the title
        setStatus(R.string.scanning)
        // If we're already discovering, stop it
        if (btAdapter!!.isDiscovering) {
            btAdapter!!.cancelDiscovery()
        }
        // Request discover from BluetoothAdapter
        btAdapter!!.startDiscovery()
    }

    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private fun setStatus(resId: Int) {
        val activity: AppCompatActivity = activity as AppCompatActivity? ?: return
        val actionBar = activity.supportActionBar ?: return
        actionBar.setSubtitle(resId)
    }

    /**
     * The BroadcastReceiver that listens for discovered devices and changes the title when
     * discovery is finished
     */
    private val receiver: BroadcastReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val action = intent.action
                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND == action) { // Get the BluetoothDevice object from the Intent
                    val device = intent.getParcelableExtra<BluetoothDevice>(
                        BluetoothDevice.EXTRA_DEVICE
                    )
                    // If it's already paired, skip it, because it's been listed already
                    if (device.bondState != BluetoothDevice.BOND_BONDED) {
                        newDevicesAdapter.add(device)
                    }
                    // When discovery is finished, change the Activity title
                } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                    showProgress(false)
                    setTitle(R.string.tap_to_select)
                    if (newDevicesAdapter.itemCount == 0) {
                        tvNoDevices?.setText(R.string.no_devices_found)
                    }
                }
            }
        }

    override fun getLayoutId(): Int = R.layout.pairing_fragment

    override fun getTitleId(): Int? = R.string.scan_to_discover

    inner class DevicesAdapter : RecyclerView.Adapter<DevicesAdapter.ViewHolder>() {

        init {
            setHasStableIds(true)
        }

        private var devices: ArrayList<DeviceData> = ArrayList()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val viewDataBinding =
                DeviceNameBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolder(viewDataBinding)
        }

        override fun getItemCount(): Int {
            return devices.size
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val device = devices[position]
            holder.dataBinding.setVariable(com.bender.btchat.BR.model, device)
            (holder.dataBinding as DeviceNameBinding).click = object : DeviceClickHandler {
                override fun onDeviceClick(deviceData: DeviceData) {
                    // Cancel discovery because it's costly and we're about to connect
                    btAdapter?.cancelDiscovery()
                    viewModel.selectDevice(deviceData)
                    goBack()
                }

            }
        }

        fun setData(devices: ArrayList<DeviceData>) {
            this.devices.clear()
            this.devices.addAll(devices)
            notifyDataSetChanged()
        }

        fun add(btDevice: BluetoothDevice) {
            val data = DeviceData(btDevice.name ?: "N/A", btDevice.address)
            devices.add(data)
            notifyItemInserted(devices.size - 1)
        }

        inner class ViewHolder(val dataBinding: ViewDataBinding) :
            RecyclerView.ViewHolder(dataBinding.root)

    }

    interface DeviceClickHandler {
        fun onDeviceClick(deviceData: DeviceData)
    }

    override fun getState(): ScreenState = ScreenState.MODAL
}

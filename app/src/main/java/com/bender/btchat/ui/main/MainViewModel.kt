package com.bender.btchat.ui.main

import android.bluetooth.BluetoothDevice
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bender.btchat.R
import com.bender.btchat.bt.BluetoothChatController
import com.bender.btchat.bt.Constants
import com.bender.btchat.bt.Constants.Companion.MESSAGE_USER_ID
import com.bender.btchat.ui.chat.ChatMessage
import com.bender.btchat.utils.SingleLiveEvent
import com.bender.firebase.FirebaseRepository
import com.bender.liba.utils.MoshiUtils
import timber.log.Timber

class MainViewModel(
    private val chatController: BluetoothChatController,
    private val firebaseRepository: FirebaseRepository
) : ViewModel() {

    val selectedDeviceData = SingleLiveEvent<DeviceData>()
    val messageEvent = SingleLiveEvent<Pair<Int, Any?>>()
    val scanButtonLiveData = SingleLiveEvent<Unit>()
    val toastResIdLiveData = SingleLiveEvent<Int>()
    val userNameLiveData = SingleLiveEvent<String>()
    var isSecureConnection: Boolean? = null
    val sendTextLiveData = MutableLiveData<String>().apply { postValue("") }
    var roomID: String = ""
    var remoteUserUid: String = ""

    init {
        chatController.messageEvent = messageEvent
    }

    fun selectDevice(deviceData: DeviceData) {
        selectedDeviceData.postValue(deviceData)
    }

    fun startChatService() {
        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (chatController.connectionState == BluetoothChatController.STATE_NONE) { // Start the Bluetooth chat services
            chatController.start()
        }
    }

    fun stopChatService() {
        chatController.stop()
    }

    private fun getConnectionState() = chatController.connectionState

    fun sendUserID() {
        val userUid = firebaseRepository.getCurrentUserId()
        chatController.write(MESSAGE_USER_ID, userUid)
    }

    fun connectToRemoteDevice(device: BluetoothDevice) {
        chatController.connect(device, isSecureConnection!!)
    }

    fun sendMessage() {
        if (getConnectionState() != BluetoothChatController.STATE_CONNECTED) {
            toastResIdLiveData.postValue(R.string.not_connected)
            return
        }
        // Check that there's actually something to send
        val message = sendTextLiveData.value
        if (message!!.isNotEmpty()) { // Get the message bytes and tell the BluetoothChatController to write

            val dbId = writeMessageToFirebase(message)
            val chatMessage = ChatMessage(dbId, message, System.currentTimeMillis())
            val chatMessageToString = MoshiUtils.toJson(chatMessage)
            chatController.write(Constants.MESSAGE_SEND, chatMessageToString)
            // Reset out string buffer to zero and clear the edit text field
            sendTextLiveData.value = ""
        }
    }

    fun setSelection(selector: View) {
        when (selector.id) {
            R.id.btnSend -> {
                sendMessage()
            }
            R.id.btnScan -> {
                scanButtonLiveData.value = Unit
            }
        }
    }

    fun generateRoomId(): String {
        val currentUserUid = firebaseRepository.getCurrentUserId()
        currentUserUid?.let {
            roomID =
                if (remoteUserUid > currentUserUid) remoteUserUid + currentUserUid else currentUserUid + remoteUserUid
        }
        return roomID
    }

    fun addRoom(id: String, name: String) {
        firebaseRepository.createRoom(id, name)
    }

    fun getUserByUID() {
        firebaseRepository.getUserByUID(remoteUserUid)
            .addOnSuccessListener {
                Timber.e("DocumentSnapshot data: ${it.data}")
                it.data?.let { data ->
                    val name = data[FirebaseRepository.KEY_USER_NAME] as String
                    userNameLiveData.postValue(name)
                }
            }
    }

    fun writeMessageToFirebase(message: String) =
        firebaseRepository.writeMessage(roomID, firebaseRepository.getCurrentUserId()!!, message)

    fun updateFirebaseMessage(chatMessage: ChatMessage) {
        firebaseRepository.updateMessage(roomID, remoteUserUid, chatMessage)
    }


    fun getMessagesQuery() = firebaseRepository.getMessagesQuery(roomID)
}
